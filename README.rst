================
ansible-dotfiles
================

Deploy dotfiles and similar

This play installs SSH keys, config, and any git repo using

- https://github.com/Oefenweb/ansible-ssh-keys
- :code:`roles/ssh-config`
- https://gitlab.com/jfriis/ansible-role-gitinstall.git

.. _usage:

Usage
=====

Create your inventory e.g. :code:`inventory` and variables e.g. :code:`vars.yml`

.. code::

   make install
   make roles
   ansible-playbook playbook.yml -e @vars.yml --skip-tags ssh-keys-known-hosts-update -i inventory


Variables
=========

For instance

.. code:: yaml

   ssh_keys_private_keys:
     - owner: "{{ ssh_owner }}"
       group: "{{ ssh_group }}"
       src: ~/.ssh/id_ed25519
       dest: id_ed25519
   ssh_keys_public_keys:
     - owner: "{{ ssh_owner }}"
       group: "{{ ssh_group }}"
       src: ~/.ssh/id_ed25519.pub
       dest: id_ed25519.pub
   ssh_keys_authorized_keys:
     - owner: "{{ ssh_owner }}"
       src: ~/.ssh/id_ed25519.pub
   gitinstalls:
     - repo: https://github.com/masasam/dotfiles.git
       key_file: ~/.ssh/id_ed25519  # if your repo is private
       targets: []  # or default to gitinstall_default_targets
   gitinstall_default_remote: false
   gitinstall_default_targets:  # make targets of git repo
     - install
   ssh_owner: "{{ lookup('env', 'USER') }}"
   ssh_group: users

