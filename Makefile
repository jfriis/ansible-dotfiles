VIRT = virt

.PHONY: all install roles modules

all: install roles modules

install:
	python3 -m venv $(VIRT)
	$(VIRT)/bin/pip install -r requirements/requirements.txt

roles: $(VIRT)
	$(VIRT)/bin/ansible-galaxy install --role-file requirements/roles.yml --roles-path roles

modules: $(VIRT)
	$(VIRT)/bin/ansible-galaxy collection install -r requirements/module_requirements.txt

html README.html:
	$(VIRT)/bin/rst2html.py README.rst README.html
